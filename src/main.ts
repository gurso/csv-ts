type Options = { headers?: string[]; delimiter?: "\n" | "\r\n" }
type O = Record<PropertyKey, string | number>

export function serialize(data: O[], { headers = Object.keys(data[0]), delimiter = "\n" }: Options = {}) {
	return [headers.join(","), ...data.map(row => headers.map(h => row[h]).join(","))].join(delimiter)
}

export function parse(csv: string, { headers, delimiter = "\n" }: Options = {}) {
	const rows = csv
		.split(delimiter)
		.filter(Boolean)
		.map(row => row.split(","))

	if (!headers) {
		headers = rows.shift() ?? []
	}

	const data: Record<string, string>[] = []

	for (const row of rows) {
		const item: Record<string, string> = {}
		for (const i in headers) {
			const h = headers[i]
			item[h] = row[i]
		}
		data.push(item)
	}
	return data
}
