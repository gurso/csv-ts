import { test, expect } from "vitest"
import { parse, serialize } from "./main"
import { readFileSync } from "fs"
import { resolve } from "path"

test("serialize function", () => {
	expect(serialize([{ test: "42" }])).toEqual("test\n42")
	expect(serialize([{ test: "42", foo: "foo" }])).toEqual("test,foo\n42,foo")
	expect(serialize([{ test: "42" }, { foo: "foo" }], { headers: ["test", "foo"] })).toEqual("test,foo\n42,\n,foo")
})

test("parse function", async () => {
	expect(parse("test\n42")).toEqual([{ test: "42" }])

	const content = readFileSync(resolve(__dirname, "./tt.csv"), { encoding: "utf8" })
	const trimedBOM = content.replace(/^\uFEFF/, "")
	const test = parse(trimedBOM, { delimiter: "\r\n" })
	expect(test).toEqual([{ foo: "42", bar: "23" }])
})
